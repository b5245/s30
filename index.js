const express = require('express');
const mongoose = require('mongoose');


const app = express();
const port = 3001;

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.mgfy3.mongodb.net/b177-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Set notification for connection success or failure
// Connection to the database
let db = mongoose.connection;

// If a connection error occured, output message in the console
// console eror bind used to print error in console terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output message in the console
db.once("open", () => console.log(`We are connected to the cloud database`))

// Create a task schema
const taskSchema = new mongoose.Schema({
    name : String,
    status: {
        type: String,
        default: "pending"
        // default values are the pre defined values for a field
    }
});
// Create Models
// Server > Schema > Database >  Collection (MongoDB)

const Task = mongoose.model("Task", taskSchema);



app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
    Task.findOne({name : req.body.name}, (err, result) => {
        // If a document was found and the document's name matches the information sent via the client/postman
        if(result !== null && result.name == req.body.name){
            // Returns a message to the client/postman
            return res.send(`Duplicate task found`)
        }
        // If no document found
        else{
            // Create a new task and save it to the database
            let newTask = new Task({
                name : req.body.name
            })
            // save method - used to store new information to the database
            newTask.save((saveErr, savedTask) => {
                if(saveErr){
                    return console.log(saveErr);
                }
                else{
                    return res.status(201).send("New task created");
                }
            })

        }
    })
})



// Create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
    Task.find({}, (err, result) => {
        // if an error occured
        if(err){
            // will print any errors found in the console
            return console.log(err);
        }
        // ifno errs found
        else{
            return res.status(200).json({
                data : result
            })
        }
    })
})


const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    status: {
        type: String,
        default: "pending"
    }
})

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
    User.findOne({username: req.body.username, password: req.body.password}, (err, result) => {
        if(req.body.username == '' || req.body.password == ''){
            return res.send(`Please input username and password`)
        }
        else if(result !== null && result.username == req.body.username){
            return res.send(`Username already used`)
        }
        else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })
            newUser.save((saveErr, savedUser) => {
                if(saveErr){
                    return console.log(saveErr)
                }
                else{
                    return res.status(201).send("New User Registered");
                }
            })
        }
    })
})

app.get("/signup", (req, res) => {
    User.find({}, (err, result) => {
        if(err){
            return console.log(err);
        }
        else{
            return res.status(200).json({
                data : result
            })
        }
    })
})


app.listen(port, () => console.log(`Server running at port ${port}`));


